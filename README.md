Ryan Zhu 2018-07-22

### To intall and run

* You'll need to have [git](https://git-scm.com/) and [node](https://nodejs.org/en/) installed in your system.
* Clone the project:

```
> $ git clone THIS_REPO_URL
```

* Then install the dependencies:

```
> $ npm install
```

* Run development server:

```
> $ npm run start-dev
```

Open the web browser to `http://localhost:3000/`


### Technical Explain

* Front-end frameworks:

```
> React + Redux + ES6 + React Router
```

* Responsive:

```
> styled-components (react) + react-flexa
```

### Architecture Explain

* The page is server side rendering and using axios for data fetch.

