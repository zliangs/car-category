import React, { Component } from 'react';
import styled from 'styled-components';
import { Container} from './Header';
import { Row, Col } from 'react-flexa';

export const FooterDiv = styled.div`
  background-color: #36363e;
  padding-top: 25px;
  color: #bbb;
  text-decoration: none;
  height: 70px;
  .footer__main {
    font-size: 13px;
    position: relative;
    min-height: 1px;
    padding-left: 0px;
    float: left;
    width: 100%;
    a {
      color: #bbb;
      text-decoration: none;
      &:hover {
        color: #bbb;
        opacity: 0.8;
        text-decoration: none;
      }
    }
  }
  
`;

const ColLink = Col.extend`
  text-align: center;
`;

export class Footer extends Component {
  render() {
     return (
      <FooterDiv>
        <Container>
            <Row>
              <ColLink xs={12} className="footer__main">
                © Car Category Limited ABN 16 009 661 901
              </ColLink>
            </Row>
          </Container>
      </FooterDiv>
    );
  }
}
