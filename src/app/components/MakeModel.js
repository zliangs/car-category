import React, { Component } from 'react';
import { selectList } from '../utils/selectList';
import styled from 'styled-components';

const StyledSelect = styled.select`
  height: 40px;
  width: 250px;
  border-radius: 0;
  padding-left: 10px;
  font-size: 18px;
  appearance: none;
  background-color: #fff;
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAh0lEQVQ4T93TMQrCUAzG8V9x8QziiYSuXdzFC7h4AcELOPQAdXYovZCHEATlgQV5GFTe1ozJlz/kS1IpjKqw3wQBVyy++JI0y1GTe7DCBbMAckeNIQKk/BanALBB+16LtnDELoMcsM/BESDlz2heDR3WePwKSLo5eoxz3z6NNcFD+vu3ij14Aqz/DxGbKB7CAAAAAElFTkSuQmCC');
  background-repeat: no-repeat;
  background-position: 225px center;
`;

export const StyledButton = styled.button`
  display: inline-block;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  -webkit-appearance: none;
  color: rgb(255, 255, 255);
  text-decoration: none;
  border-radius: 100px;
  outline: 0px;
  border-width: initial;
  border-style: none;
  border-color: initial;
  border-image: initial;
  background: rgb(0, 185, 189);
  padding: 12px 40px;
`;

export class MakeModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      makeSelected: 'Please Select Make',
      modelSelected: ''
    };
  }

  handleMakeChange(e) {
    this.setState({ makeSelected: e.target.value });
  }

  handleModelChange(e) {
    this.setState({ modelSelected: e.target.value });
  }
  handleMakeBlur(e) {
    this.props.parentAction({ make: e.target.value });
  }

  handleModelBlur(e) {
    this.props.parentAction({ model: e.target.value });
  }

  render() {
    const { carList, makes } = this.props;
    const priciList = carList ? selectList(this.props.carList) : [];

    return priciList && priciList.length > 0 ? (
      <div>
        <StyledSelect
          onChange={this.handleMakeChange.bind(this)}
          onBlur={this.handleMakeBlur.bind(this)}
        >
          {priciList.map((item, key) => (
            <option value={item.make} key={key}>
              {makes.find(x => x.id == item.make)
                ? makes.find(x => x.id == item.make).name
                : item.make}
            </option>
          ))}
        </StyledSelect>
        <span style={{ marginLeft: '20px' }} />
        <StyledSelect
          onChange={this.handleModelChange.bind(this)}
          onBlur={this.handleModelBlur.bind(this)}
        >
          {this.state.makeSelected != 'Please Select Make' && (
            <option value="0" selected>
              All
            </option>
          )}
          {priciList
            .find((item, key) => item.make == this.state.makeSelected)
            .models.map((item, key) => {
              const optionItem = item.split('-');
              return (
                <option value={optionItem[0]} key={key}>
                  {optionItem.length == 2 ? optionItem[1] : item}
                </option>
              );
            })}
        </StyledSelect>
      </div>
    ) : (
      <div>loading</div>
    );
  }
}
