import React, { Component } from 'react';
import { Container } from '../components/Header';
import { Row, Col } from 'react-flexa';
import styled from 'styled-components';
import { MakeModel } from './MakeModel';
import { filterList } from '../utils/selectList';
import { Link } from 'react-router-dom';

const Card = styled.div`
  border-radius: 10px;
  border: 1px #eee solid;
  background: #fff;
  padding: 20px;
  margin: 20px;
  img {
    max-width: 480px;
    width: 100%;
    height: auto;
  }
  a {
    color: #7b7c7e;
    text-decoration: none;
    font-weight: 400;
    font-size: 16px;
    line-height: 26px;
    display: inline-block;
  }
`;

const ItemTitle = styled.div`
  font-size: 20px;
  color: #0d5257;
  line-height: 26px;
  margin-bottom: 10px;
`;

const ItemText = styled.div`
  color: #555659;
  font-size: 14px;
  line-height: 26px;
`;

const Bar = styled.div`
  background-color: #36363e;
  padding-top: 20px;
  height: 60px;
  color: #fff;
  font-size: 20px;
`;

export const StyledButton = styled.button`
  display: inline-block;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  -webkit-appearance: none;
  color: rgb(255, 255, 255);
  text-decoration: none;
  border-radius: 5px;
  outline: 0px;
  border-width: initial;
  border-style: none;
  border-color: initial;
  border-image: initial;
  background: #e00;
  padding: 10px 40px;
`;

export const StyledButtonDisabled = styled.button`
  display: inline-block;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  -webkit-appearance: none;
  color: #fff;
  text-decoration: none;
  border-radius: 5px;
  outline: 0px;
  border-width: initial;
  border-style: none;
  border-color: initial;
  border-image: initial;
  background: rgba(67, 68, 71, 0.5);
  padding: 10px 40px;
`;

export class CarList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      make: '',
      model: ''
    };
  }

  handleParentAction(value) {
    value.model ? this.setState({ model: value.model }) : this.setState({ make: value.make });
  }

  handleClick() {
    const { carList } = this.props;
    const newlist = filterList(this.state.make, this.state.model, carList);
    console.log(this.state.make, this.state.model);
    this.setState({ list: newlist });
  }

  render() {
    const { list, make } = this.state;
    const { carList, makes } = this.props;
    return (
      <div style={{ minHeight: '500px' }}>
        <Bar>
          <Container>
            <Row>
              <Col xs={12}>Car Catalog</Col>
            </Row>
          </Container>
        </Bar>
        <Container mb={1.2}>
          <Row>
            <Col style={{ margin: '20px' }}>
              <MakeModel
                parentAction={this.handleParentAction.bind(this)}
                carList={carList}
                makes={makes}
              />
            </Col>
            <Col style={{ margin: '20px 0' }}>
              {make ? (
                <StyledButton value="View" type="button" onClick={this.handleClick.bind(this)}>
                  View
                </StyledButton>
              ) : (
                <StyledButtonDisabled>View</StyledButtonDisabled>
              )}
            </Col>
          </Row>
          <Row>
            {list &&
            list.map(dataItem => {
              return (
                <Col xs={12} md={6} key={dataItem.id}>
                  <Card snow>
                    <Row>
                      <Col xs={12}>
                        <Link to={`/make/model/${dataItem.id}`}>
                          <img src={dataItem.imageUrl} />
                        </Link>
                        <ItemTitle>
                          {makes.find(x => x.id == dataItem.makeId)
                            ? makes.find(x => x.id == dataItem.makeId).name
                            : dataItem.makeId}
                        </ItemTitle>
                        <ItemText>{dataItem.name}</ItemText>
                        <ItemText>{dataItem.price}</ItemText>
                      </Col>
                      <Col xs={12}>
                        <Link to={`/make/model/${dataItem.id}`}>View</Link>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </Container>
      </div>
    );
  }
}
