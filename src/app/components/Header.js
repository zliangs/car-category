import React, { Component } from 'react';
import { Row, Col } from 'react-flexa';
import styled from 'styled-components';
import { Link } from "react-router-dom";

export const Container = styled.div`
  margin-left: auto;
  margin-right: auto;

  padding-left: 20px;
  padding-right: 20px;
  position: relative;
  @media (min-width: 0rem) {
    max-width: 30rem;
  }
  @media (min-width: 30rem) {
    max-width: 40rem;
  }
  @media (min-width: 48rem) {
    max-width: 60rem;
  }
  @media (min-width: 60rem) {
    max-width: 1140px;
  }
`;

export const Logo = styled.a`
  display: inline-block;
  margin: 0.5rem 1rem;
  div {
    width: 120px;
    height: 40px;
    background: url('https://www.qantas.com/etc/designs/qantas/global/img/qantas.svg') no-repeat;
    display: inline-block;
    vertical-align: middle;
    margin-top: 1rem;
  }
`;

export const HeaderLink = styled.div`
  text-align: left;
  font-size: 1rem;
  vertical-align: middle;
  margin-right: 10px;
  margin-left: 20px;
  display: inline-block;
  
    color: #777;
    text-decoration: none;
    background-color: transparent;
  
  &:focus,
  :hover {
    color: #e00;
    text-decoration: none;
  }
  &:focus {
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
  }
  &:active,
  :hover {
    outline-width: 0;
  }
`;

export class Header extends Component {
  render() {
    return (
      <div style={{ background: '#fff' }}>
        <Container>
          <Row>
            <Col xs={12}>
              <Logo href="/" rel="noopener noreferrer">
                <div />
              </Logo>
              <Link to="/">
                <HeaderLink>
                  Home
                </HeaderLink>
              </Link>
              <Link to="/search">
                <HeaderLink>
                  Search
                </HeaderLink>
              </Link>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
