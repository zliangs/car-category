import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import reducers from '../reducers';
import thunk from 'redux-thunk';
import promise from 'redux-promise';

export default (state = {}, history) => {
  const middleware = applyMiddleware(thunk, promise, routerMiddleware(history));

  const createStoreWithMiddleware = compose(
    middleware,
    typeof window !== 'undefined' && window.devToolsExtension ? window.devToolsExtension() : f => f
  );

  return createStoreWithMiddleware(createStore)(reducers, state);
};
