import Home from "./containers/Home";
import Car from "./containers/Car";
import Cars from "./containers/Cars";

const routes = [
  {
    path: "/",
    exact: true,
    component: Home
  },
  {
    path: "/search",
    component: Cars
  },
  {
    path: "/make/model/:id",
    component: Car
  }
];

export default routes;
