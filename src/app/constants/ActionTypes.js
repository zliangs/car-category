export default {
  FETCH_CARS_REQUEST: 'FETCH_CARS_REQUEST',
  FETCH_CARS_SUCCESS: 'FETCH_CARS_SUCCESS',
  FETCH_CARS_FAILURE: 'FETCH_CARS_FAILURE',
  FETCH_CAR_OF_WEEK_SUCCESS: 'FETCH_CAR_OF_WEEK_SUCCESS',
  FETCH_CAR_OF_WEEK_FAILURE: 'FETCH_CAR_OF_WEEK_FAILURE',
  FETCH_MAKES_SUCCESS: 'FETCH_MAKES_SUCCESS',
  FETCH_MAKES_FAILURE: 'FETCH_MAKES_FAILURE'
};
