import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import routes from "./routes";
import { Header } from './components/Header';
import { Footer } from './components/Footer';
import { injectGlobal } from 'styled-components';
import { normalize } from 'polished';

injectGlobal`
  ${normalize()}
  * {
    min-height: 0;
    min-width: 0;
    box-sizing: border-box;
  }
  body {
    background: #fff;
  }

  html, body, button, input, optgroup, select, textarea {
    font-family: "Ciutadella",Helvetica,Arial,sans-serif;
  }
`;

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        {routes.map((route, i) => <Route key={i} {...route} />)}
        <Footer />
      </div>
    );
  }
}

export default App;
