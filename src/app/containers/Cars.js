import React, { Component } from 'react';
import { CarList } from '../components/CarList';
import {fetchCarOfWeek, fetchCars, fetchMakes} from '../actions/cars';
import { connect } from 'react-redux';


class Cars extends Component {

  static initialAction() {
    return fetchCars();
  }

  componentDidMount() {
    if (!this.props.carList) {
      this.props.dispatch(Cars.initialAction());
    }
    if (!this.props.makes) {
      this.props.dispatch(fetchMakes());
    }
  }

  render() {
    const { carList, makes } = this.props;
    return (
      <CarList carList={carList} makes={makes} />
    );
  }
}

const mapStateToProps = state => ({
  carList: state.carList,
  makes: state.makes
});

export default connect(mapStateToProps)(Cars);
