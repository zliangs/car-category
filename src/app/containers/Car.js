import React, { Component } from "react";
import { connect } from "react-redux";
import { Container} from '../components/Header';
import { Row, Col } from 'react-flexa';
import styled from 'styled-components';

const Bar = styled.div`
  background-color: #36363e;
  padding-top: 20px;
  height: 60px;
  color: #fff;
  font-size: 20px; 
`;

const Card = styled.div`
  border-radius: 10px;
  border: 1px #eee solid;
  background: #fff;
  padding: 20px;
  margin: 40px 20px;
`;

const ItemTitle = styled.div`
  font-size: 20px;
  color: #0d5257;
  line-height: 26px;
  margin-bottom: 10px;
`;

const ItemText = styled.div`
  color: #555659;
  font-size: 14px;
  line-height: 26px;
`;

const ImgCover = styled.img`
  width: 100%;
  max-width: 800px;
  height: auto;
`;


class Car extends Component {
  render() {
    const { carList, modelId, makes } = this.props;
    const dataItem = carList && carList.find(x => x.id == modelId);
    return (
      <div style={{ minHeight: '500px'}}>
        <Bar>
          <Container>
            <Row>
              <Col xs={12}>
                Selected Model
              </Col>
            </Row>
          </Container>
        </Bar>
        <Container>
          <Row>
            <Col xs={12}>
              <Card>
                <ImgCover src={dataItem.imageUrl} />
                <ItemTitle>
                  {makes.find(x => x.id == dataItem.makeId) ? makes.find(x => x.id == dataItem.makeId).name : dataItem.makeId}
                </ItemTitle>
                <ItemText>{dataItem.name}</ItemText>
                <ItemText>{dataItem.price}</ItemText>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  carList: state.carList ? state.carList : [],
  makes: state.makes ? state.makes : [],
  modelId: props.match.params.id
});

export default connect(mapStateToProps)(Car);
