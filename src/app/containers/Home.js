import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCarOfWeek } from '../actions/cars';
import { Container } from '../components/Header';
import { Row, Col } from 'react-flexa';
import styled from 'styled-components';

const Bar = styled.div`
  background-color: #36363e;
  padding-top: 20px;
  height: 60px;
  color: #fff;
  font-size: 20px;
`;

const Card = styled.div`
  border-radius: 10px;
  border: 1px #eee solid;
  background: #fff;
  padding: 20px;
  margin: 40px 20px;
`;

const ImgCover = styled.img`
  width: 100%;
  max-width: 1000px;
  height: auto;
`;

class Home extends Component {
  static initialAction() {
    return fetchCarOfWeek();
  }

  componentDidMount() {
    if (!this.props.carofweek) {
      this.props.dispatch(Home.initialAction());
    }
  }

  render() {
    const { carofweek } = this.props;
    return (
      <div style={{ minHeight: '500px' }}>
        <Bar>
          <Container>
            <Row>
              <Col xs={12}>Car of The Week</Col>
            </Row>
          </Container>
        </Bar>
        <Container>
          <Row>
            <Col xs={12}>
              <Card>
                <ImgCover src="https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5-ipm/mx-5-at-one-with-the-road.jpg" />
                <div style={{ margin: '20px 0' }}>Model: {carofweek && carofweek.modelId}</div>
                <div style={{ margin: '20px 0' }}>{carofweek && carofweek.review}</div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  carofweek: state.carofweek
});

export default connect(mapStateToProps)(Home);
