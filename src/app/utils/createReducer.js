/**
 * Creates a hash of reducers - used to get rid of switch syntax
 * @param initialState
 * @param handlers
 * @returns {Object}
 * // TODO - We have this in every repo, move it somewhere
 */
export default function createReducer(initialState, handlers) {
  return function reducer(state = initialState, action) {
    if (handlers.hasOwnProperty(action.type)) {
      return handlers[action.type](state, action);
    }
    return state;
  };
}

export const setStatusForKey = (state, { type, payload }) =>
  Object.assign({}, state, { [payload.id]: type });

export function mergeById(state, { payload }) {
  const old = state[payload.id] || {};
  state[payload.id] = { ...old, ...payload };
  return { ...state };
}

export function createCollectionStatusReducer(...statues) {
  return createReducer(
    {},
    statues.reduce((handlers, status) => {
      handlers[status] = setStatusForKey;
      return handlers;
    }, {})
  );
}

export function createCollectionReducer(...statues) {
  return createReducer(
    {},
    statues.reduce((handlers, status) => {
      handlers[status] = mergeById;
      return handlers;
    }, {})
  );
}
