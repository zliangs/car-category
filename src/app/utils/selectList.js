export function selectList(cat) {
  const result = {};
  cat.forEach((item) => {
    const makeId = item.makeId;
    if(result[makeId]){
      const models = result[makeId];
      const model = `${item.id}-${item.name}`;

      if(models.indexOf(model) === -1){
        models.push(model);
        result[item.makeId] = models;
      }
    }else{
      const models = [];
      models.push(`${item.id}-${item.name}`);
      result[makeId] = models;
    }
  });
  const list = createOptionForList(result);
  const final = [{
    make: 'Please Select Make',
    models: ['Please Select Model']
  }, ...list];

  return final;
}

const createOptionForList = list =>
  Object.keys(list).map(key => ({
    make: key,
    models: list[key]
  }));


export function filterList(make, model, list) {
  if (make && model > 0) {
    return list.filter(item => item.makeId == make && item.id == model);
  } else if (make && model == 0){
    return list.filter(item => item.makeId == make);
  } else {
    return list;
  }
}
