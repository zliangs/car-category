import axios from 'axios';
import ActionTypes from '../constants/ActionTypes';

const api = axios.create({
  baseURL:
    typeof SERVER === 'undefined' || SERVER // eslint-disable-line no-undef
      ? `http://localhost:${typeof PORT === 'undefined' ? '3000' : PORT}` // eslint-disable-line no-undef
      : '/'
});

// Action Creators
const requestCars = () => ({ type: ActionTypes.FETCH_CARS_REQUEST });
const receivedCars = news => ({ type: ActionTypes.FETCH_CARS_SUCCESS, payload: news });
const carsError = () => ({ type: ActionTypes.FETCH_CARS_FAILURE });

const receivedCarOfWeek = carofweek => ({ type: ActionTypes.FETCH_CAR_OF_WEEK_SUCCESS, payload: carofweek });
const carOfWeekError = () => ({ type: ActionTypes.FETCH_CAR_OF_WEEK_FAILURE });

const receivedMakes = makes => ({ type: ActionTypes.FETCH_MAKES_SUCCESS, payload: makes });
const makesError = () => ({ type: ActionTypes.FETCH_MAKES_FAILURE });

export const fetchCars = () => {
  return async dispatch => {
    try {
      dispatch(requestCars());
      const { data: carList } = await api.get(`http://localhost:3000/api/cars`);
      dispatch(receivedCars(carList));
    } catch (e) {
      dispatch(carsError());
    }
  };
};

export const fetchMakes = () => {
  return async dispatch => {
    try {
      const { data: makes } = await api.get(`http://localhost:3000/api/makes`);
      dispatch(receivedMakes(makes));
    } catch (e) {
      dispatch(makesError());
    }
  };
};

export const fetchCarOfWeek = () => {
  return async dispatch => {
    try {
      const { data } = await api.get(`http://localhost:3000/api/carofweek`);
      dispatch(receivedCarOfWeek(data));
    } catch (e) {
      dispatch(carOfWeekError());
    }
  };
};

