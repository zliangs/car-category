import ActionTypes from '../constants/ActionTypes';
export default function reducer(state = {}, action) {
  switch (action.type) {
    case ActionTypes.FETCH_CARS_SUCCESS:
      return { ...state, carList: action.payload };

    case ActionTypes.FETCH_CAR_OF_WEEK_SUCCESS:
      return { ...state, carofweek: action.payload };

    case ActionTypes.FETCH_MAKES_SUCCESS:
      return { ...state, makes: action.payload };

    default:
      return state;
  }
}
